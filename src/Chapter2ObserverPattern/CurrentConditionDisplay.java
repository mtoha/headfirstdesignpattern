/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chapter2ObserverPattern;

/**
 *
 * @author TOHA
 */
public class CurrentConditionDisplay implements Observer, DisplayElement{
    private float temperature;
    private float humadity;
    private Subject weatherData;

    public CurrentConditionDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }
    
    @Override
    public void update(float temp, float humadity, float pressure) {
        this.temperature = temp;
        this.humadity = humadity;
        display();
    }

    @Override
    public void display() {
        System.out.println("Current conditions : "+ temperature+ "F degrees and "+ humadity+"% humidity");
    }
    
}
