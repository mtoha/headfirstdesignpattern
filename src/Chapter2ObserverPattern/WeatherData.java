/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chapter2ObserverPattern;

import java.util.ArrayList;

/**
 *
 * @author TOHA
 */
public class WeatherData implements Subject {

    private ArrayList observers;
    private float temperature;
    private float humadity;
    private float pressure;

    public WeatherData() {
        observers = new ArrayList();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        int i = observers.indexOf(o);
        if (i >= 0) {
            observers.remove(i);
        }
    }

    @Override
    public void notifyObservers() {
        for (int i = 0; i < observers.size(); i++) {
            Observer observer = (Observer) observers.get(i);
            observer.update(temperature, humadity, pressure);
        }
    }

    public void measurementsChanged(){
        notifyObservers();
    }
    
    public void setMeasurements(float temperature, float humadity, float pressure){
        this.temperature = temperature;
        this.humadity = humadity;
        this.pressure = pressure;
        measurementsChanged();
    }
}
